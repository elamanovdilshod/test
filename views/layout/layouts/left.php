<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/users']],
                    ['label' => 'Проекты', 'icon' => 'file-code-o', 'url' => ['/project']],
                    
                    
                ],
            ]
        ) ?>

    </section>

</aside>
