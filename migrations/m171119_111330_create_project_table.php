<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m171119_111330_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'cost' => $this->integer()->notNull(),
            'date_start' => $this->date(),
            'date_end' => $this->date(),
        ]);

        $this->createIndex('idx-project-user_id', 'project', 'user_id', false);
        $this->addForeignKey("fk-project-user_id", "project", "user_id", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-project-user_id','project');
        $this->dropIndex('idx-project-user_id','project');

        $this->dropTable('project');
    }
}
