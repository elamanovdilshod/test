<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m171119_100822_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->notNull(),
            'login' => $this->string(255)->notNull(),
            'parol' => $this->string(255)->notNull(),
        ]);

        $this->insert('users',array(
            'fio' => 'admin',
            'login' => 'admin',
            'parol' => 'admin',
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
